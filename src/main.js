import Vue from 'vue'
import App from './App.vue'
import router from './router'
import VAnimateCss from 'v-animate-css';
import ElementUI from 'element-ui';
import locale from 'element-ui/lib/locale/lang/en'

import Checkbox from '@/components/form-fields/Checkbox.vue';

import UILink from '@/components/ui/UILink.vue';
import UIButton from '@/components/ui/UIButton.vue';
import UITable from '@/components/ui/UITable.vue';
import UITextarea from '@/components/ui/UITextarea.vue';
import UIInput from '@/components/ui/UIInput.vue';
import UIInputRadio from '@/components/ui/UIInputRadio.vue';
import UIInputRadioGroup from '@/components/ui/UIInputRadioGroup.vue';
import UIInputPassword from '@/components/ui/UIInputPassword.vue';
import UIInputPasswordWithConfirm from '@/components/ui/UIInputPasswordWithConfirm.vue';
import UIInputDate from '@/components/ui/UIInputDate.vue';
import UIInputMask from '@/components/ui/UIInputMask.vue';
import UIInputPhone from '@/components/ui/UIInputPhone.vue';
import UIInputEmail from '@/components/ui/UIInputEmail.vue';
import UISelect from '@/components/ui/UISelect.vue';
import UICurency from '@/components/ui/UICurency.vue';
import UISelectRepeated from '@/components/ui/UISelectRepeated.vue';
import UISelectExpand from '@/components/ui/UISelectExpand.vue';
// import UISelectUserChange from '@/components/ui/UISelectUserChange.vue';
import UISelectAgentChange from '@/components/ui/UISelectAgentChange.vue';
import UIIcon from '@/components/ui/UIIcon.vue';
import UITooltip from '@/components/ui/UITooltip.vue';
import UITooltipQuestion from '@/components/ui/UITooltipQuestion.vue';
import UIPopup from '@/components/ui/UIPopup.vue';
import UIMenuTopButton from '@/components/ui/UIMenuTopButton.vue';
import UIMenuTopDropdown from '@/components/ui/UIMenuTopDropdown.vue';
// import UIMenuTopCreate from '@/components/ui/UIMenuTopCreate.vue';
import UIMenuTopLogo from '@/components/ui/UIMenuTopLogo.vue';
import UIInfoCard from '@/components/ui/UIInfoCard.vue';
import UICompanyAvatar from '@/components/ui/UICompanyAvatar.vue';
import UIContactAvatar from '@/components/ui/UIContactAvatar.vue';
import UIMagnifyingImage from '@/components/ui/UIMagnifyingImage.vue';
import UIHr from '@/components/ui/UIHr.vue';
import UIH2 from '@/components/ui/UIH2.vue';
import UIH3 from '@/components/ui/UIH3.vue';
import UIH4 from '@/components/ui/UIH4.vue';
import UISpoiler from '@/components/ui/UISpoiler.vue';
import UIControlsBlock from '@/components/ui/UIControlsBlock.vue';
import UIPagination from '@/components/ui/UIPagination.vue';
import UIMap from '@/components/ui/UIMap.vue';

import UIInputOGRN from '@/components/ui/UIInputOGRN.vue';
import UIInputINNKPP from '@/components/ui/UIInputINNKPP.vue';
import UIInputRS from '@/components/ui/UIInputRS.vue';
import UIInputKORS from '@/components/ui/UIInputKORS.vue';
import UIInputBIC from '@/components/ui/UIInputBIC.vue';
import UIInputSearch from '@/components/ui/UIInputSearch.vue';
import UIInputAddress from '@/components/ui/UIInputAddress.vue';

import UIInputPassportSeriesNumber from '@/components/ui/UIInputPassportSeriesNumber.vue';
import UIInputPassportUnitCode from '@/components/ui/UIInputPassportUnitCode.vue';

import UIBlockPassport from '@/components/ui/UIBlockPassport.vue';
import UIMessengers from '@/components/ui/UIMessengers.vue';

import UIInfo from '@/components/ui/UIInfo.vue';
import UIInfoPhone from '@/components/ui/UIInfoPhone.vue';
import UIInfoMessengers from '@/components/ui/UIInfoMessengers.vue';
import UIInfoSkype from '@/components/ui/UIInfoSkype.vue';
import UIInfoMoney from '@/components/ui/UIInfoMoney.vue';
import UIInfoOfferQty from '@/components/ui/UIInfoOfferQty.vue';
import UIInfoDemandQty from '@/components/ui/UIInfoDemandQty.vue';
import UIInfoEmail from '@/components/ui/UIInfoEmail.vue';
import UIInfoSite from '@/components/ui/UIInfoSite.vue';
import UIInfoPassport from '@/components/ui/UIInfoPassport.vue';

import UIControlUserPhones from '@/components/ui/control/UIControlUserPhones.vue';
import UIIsActiveControl from '@/components/ui/control/UIIsActiveControl.vue';
import UIActivityControl from '@/components/ui/control/UIActivityControl.vue';
import UIImageControl from '@/components/ui/control/UIImageControl.vue';
import UIFilesControl from '@/components/ui/control/UIFilesControl.vue';
import UIOneOfControl from '@/components/ui/control/UIOneOfControl.vue';
import UIControl from '@/components/ui/control/UIControl.vue';
import UISearchControl from '@/components/ui/control/UISearchControl.vue';
import UIPaginationControl from '@/components/ui/control/UIPaginationControl.vue';


import BlockUserChange from '@/components/block/BlockUserChange.vue';
import BlockAgentChange from '@/components/block/BlockAgentChange.vue';
import BlockClientChange from '@/components/block/BlockClientChange.vue';
import BlockContract from '@/components/block/BlockContract.vue';

import Upload from '@/components/form-fields/Upload.vue';


import filters from '@/common/filters.js';


import 'element-ui/lib/theme-chalk/index.css';



Vue.use(ElementUI, { locale })
Vue.use(VAnimateCss);
Vue.config.productionTip = false

Vue.component('UISelect', UISelect);


new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
