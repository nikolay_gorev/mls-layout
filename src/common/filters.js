import IMask from 'imask';
import {addMoneySpaces} from '@/common/utils.js';


var mask = IMask(document.createElement('input'), {mask: ''});

export default {
	phone (value) {
		if (!value) return '';
		
		mask.updateOptions({
			mask: '+0-000-000-00-00',
		});
		mask.unmaskedValue = value + '';
		
		return mask.value;
	},
	
	serverDate(value){
		return value && value.split('.').reverse().join('.');
	},
	
	money(value){
		return addMoneySpaces(value);
	}
};