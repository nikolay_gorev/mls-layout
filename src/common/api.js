let loadMapPromise;
export const loadMap = () => {
	if (!loadMapPromise) {
		loadMapPromise =  new Promise(function load (resolve) {
			let yScript = document.createElement('script');
			yScript.src = URL_YMAPS;
			document.documentElement.firstElementChild.appendChild(yScript);
			
			yScript.addEventListener('load', () => {
				window.ymaps.ready(resolve);
			});
			
			yScript.addEventListener('error', () => {
				console.log('map load error');
				yScript.parentNode.removeChild(yScript);
				load(resolve);
			});
		})
	}
	
	return loadMapPromise
}
