export const OL_ZONES_FOR_ZOOMS = [
	{ from: 0, to: 8, isShown: false,},
	{ from: 9, to: 9, isShown: false,},
	{ from: 10, to: 12, isShown: true, isFill: false },
	{ from: 13, to: 20, isShown: false,},
];

export const OL_CITY_DISTRICTS_FOR_ZOOMS = [
	{ from: 0, to: 8, isShown: false,},
	{ from: 9, to: 9, isShown: true, isFill: false },
	{ from: 10, to: 12, isShown: true, isFill: true },
	{ from: 13, to: 20, isShown: false,},
];

export const OL_REGION_DISTRICTS_FOR_ZOOMS = [
	{ from: 0, to: 6, isShown: false,},
	{ from: 7, to: 9, isShown: true, isFill: true},
	{ from: 10, to: 20, isShown: false,},
];

export const OL_REGION_CITIES_FOR_ZOOMS = [
	{ from: 0, to: 6, isShown: false,},
	{ from: 7, to: 9, isShown: true, isFill: true},
	{ from: 10, to: 20, isShown: false,},
];

export const OL_DIRECTIONS_FOR_ZOOMS = [
	{ from: 0, to: 6, isShown: false,},
	{ from: 7, to: 9, isShown: true},
	{ from: 10, to: 20, isShown: false,},
];

export const OL_REGIONS_FOR_ZOOMS = [
	{ from: 0, to: 3, isShown: false,},
	{ from: 4, to: 6, isShown: true, isFill: true},
	{ from: 7, to: 20, isShown: false},
];


/*
*/
export const OL_OBJECTS_FOR_ZOOMS = [
	// 3 – 0	– не отображаем ничего
	{
		from: 0,
		to: 3,
		sortBy: {}
	},
	
	// 6 – 4	– если в регионе есть хоть 1 объект, то указываем количество объектов в регионе, если нет - ничего не указываем
	{
		from: 4,
		to: 6,
		sortBy: {
			regions: {
				placemarksMax: 0
			}
		}
	},
	
	// 9 - 7	– если в город (столицу региона) или район области внутри региона попадает менее 7 объектов, то в городе (столице региона) или районе области отображаем объекты по адресам, если более - указываем количество объектов в городе (столице региона) или районе области
	{
		from: 7,
		to: 9,
		sortBy: {
			regionDistricts: {
				placemarksMax: 7
			},
			
			regionCities: {
				placemarksMax: 7
			}
		},
	},
	
	// 10	– если в район города или в район области попадает менее 7 объектов, то в районе отображаем объекты по адресам, если более - указываем количество объектов в районе
	{
		from: 10,
		to: 10,
		sortBy: {
			cityDistricts: {
				placemarksMax: 7,
				clusterize: true
			},
			
			regionDistricts: {
				placemarksMax: 7,
				clusterize: true
			},
		}
	},
	
	// 12 – 11	– для города, если в зону попадает менее 7 объектов, то в зоне отображаем объекты по адресам, если более - указываем количество объекта в зоне; для области оставить то, что сделано
	{
		from: 11,
		to: 12,
		sortBy: {
			zones: {
				placemarksMax: 7,
				clusterize: true
			},
			
			regionDistricts: {
				placemarksMax: Infinity,
				clusterize: true
			}
		}
	},
	
	// 19 – 13	– оставить то, что сделано (если объекты в одном доме, делаем сдвиг на несколько пикселей)
	{
		from: 13,
		to: 20,
		sortBy: {
			zones: {
				placemarksMax: Infinity,
				clusterize: true
			},
			
			regionDistricts: {
				placemarksMax: Infinity,
				clusterize: true
			}
		}
	},
];


export const OL_ZOOMS = {
	// При изменении масштаба:
	CITY_FROM: 10,
	REGION_BEFORE: 9,
	COUNTRY_BEFORE: 6,
	
	// Новая структура для CITY_FROM, REGION_BEFORE, COUNTRY_BEFORE:
	CITY: {FROM: 10, TO: 20},
	REGION: {FROM: 7, TO: 9},
	COUTRY: {FROM: 0, TO: 6},
	
	// При переключении селекта город/область:
	DEFAULT_FOR_CITY: 11,
	DEFAULT_FOR_REGION: 8,
	
	/*DISPLAY_MODE_REGIONS_FROM: 0,
	DISPLAY_MODE_DIRECTIONS_FROM: 5,
	DISPLAY_MODE_REGION_DISTRICTS_FROM: 7,
	DISPLAY_MODE_CITY_DISTRICTS_FROM: 10,
	DISPLAY_MODE_ZONES_FROM: 12,
	DISPLAY_MODE_OBJECTS_FROM: 13,*/
}