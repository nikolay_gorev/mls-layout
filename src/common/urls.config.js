export const DOMAIN = '';
const DOMAIN_TEST = '';


export const URL_URLS = '';


export const URL_PAGE_OBJECT_CREATE = '';
export const URL_PAGE_OBJECT_READ = '';
export const URL_PAGE_OBJECT_UPDATE = '';
export const URL_OBJECTS_FILTER = '';




// Предложения
// Квартиры / Комнаты / Дома и земельные участки / Коммерческая недвижимость / Аренда квартир / Гаражи.
export const URL_PAGE_FILTER = '';
export const URL_PAGE_FILTER_LIST = '';
export const URL_PAGE_SALE_FILTER_APARTMENT = '';
// export const URL_PAGE_SALE_FILTER_ROOM = '';
// export const URL_PAGE_SALE_FILTER_HOUSE = '';
// export const URL_PAGE_SALE_FILTER_COMMERCIAL = '';
// export const URL_PAGE_SALE_FILTER_GARAGE = '';

// export const URL_PAGE_RENT_FILTER_APARTMENT = '';

export const URL_PAGE_SALE_CREATE = '';
export const URL_PAGE_SALE_READ = '';
export const URL_PAGE_SALE = '';



// Заявки
export const URL_PAGE_BUY = '';
// Реклама
export const URL_PAGE_AD = '';
// Агенства
export const URL_PAGE_COMPANY_LIST = '';
// export const URL_PAGE_COMPANY_CREATE = '';
// Агенты
export const URL_PAGE_AGENT_LIST = '';
// export const URL_PAGE_AGENT_CREATE = '';
// Клиенты
export const URL_PAGE_CLIENT_LIST = '';
// export const URL_PAGE_CLIENT_CREATE = '';



export const URL_YMAPS = '';

export const URL_USER_LOGIN = '';

export const URL_INITIAL = '';

// http://podstol.mlsoptima.ru/api/v2/address/hint?address= ''1
export const URL_ADDRESSES_VARINATS = '';



// /api/v2/agents/<string:whose> my department company
export const URL_USERS_FOR_CHANGE = '';




// OBJECT CARD

export const URL_OC_VARIANTS = '';
export const URL_OC_GEO_VARIANTS = '';

export const URL_OC_PHOTOS_SAVE = '';
export const URL_OC_PHOTOS_DELETE = '';

export const URL_OC_LAYOUT_SAVE = '';
export const URL_OC_LAYOUT_DELETE = '';

export const URL_OC_DOCS_SAVE = '';
export const URL_OC_DOCS_DELETE = '';

export const URL_OC_APPLY = '';

export const URL_OC_OBJECT_EDIT = '';


export const URL_ICON_PDF = '';
export const URL_ICON_WORD = '';
// https://sprawnyprawnik.files.wordpress.com/2016/12/microsoft_word_2013_logo_400_pix.png?w= '';
export const URL_ICON_EXCEL = '';


// OBJECTS LIST
export const URL_OL_LIST = '';
export const URL_OL_ZONES_DISTRICTS = '';
export const URL_OL_SORT = '';
export const URL_OLF_GEO_VARIANTS = '';
export const URL_OL_GEO_FOR_POINT = '';

// export const URL_OLF_VARIANTS = '';
export const URL_OLF_VARIANTS_COMMON = '';
export const URL_OLF_VARIANTS_APARTMENT_SALE = '';
export const URL_OLF_VARIANTS_APARTMENT_RENT = '';

export const URL_OLF_FILTERS_SAVE = '';
export const URL_OLF_FILTERS_SAVED_LIST = '';
// export const URL_OLF_FILTER_SAVED_VALUES = '';
export const URL_OLF_FILTER_SAVED_VALUES = '';
export const URL_OLF_FILTER_DELETE = '';

export const URL_OLF_LIST_SAVE = '';
export const URL_OLF_LISTS = '';
export const URL_OLF_LIST_DELETE = '';

export const URL_OLF_ADDS_VARIANTS = '';

/*
lat = ''и
long = ''и
http://podstol.mlsoptima.ru/api/point_belonging?latitude= ''4
*/
export const URL_OLF_POINT_BELONGING = '';

// export const URL_OL_LIST = '';


// OBJECT
export const URL_O = '';


/*
http://phab.mlsoptima.ru/T1670
	Страница ввода компании
	http://podstol.mlsoptima.ru/api/company/add/page

	Страница ввода агента
	http://podstol.mlsoptima.ru/api/agent/add/page

	Страница ввода клиента
	http://podstol.mlsoptima.ru/api/client/add/page
	

http://phab.mlsoptima.ru/T1672
	http://podstol.mlsoptima.ru/api/client/add

http://phab.mlsoptima.ru/T1673
	http://podstol.mlsoptima.ru/api/company/add

http://phab.mlsoptima.ru/T1674
	http://podstol.mlsoptima.ru/api/agent/list
	http://podstol.mlsoptima.ru/api/client/list
	http://podstol.mlsoptima.ru/api/company/list

*/





// COMPANY
// http://podstol.mlsoptima.ru/api/v2/company/list?limit= ''1
export const URL_COMPANY_LIST = '';
export const URL_COMPANY_CREATE = '';
//  /api/company/edit/<int:company_id>
export const URL_COMPANY_UPDATE = '';
// /api/v2/company/edit/<int:company_id>
export const URL_COMPANY_UPDATE_PARAMS = '';
export const URL_COMPANY_CREATE_PARAMS = '';
// export const URL_COMPANY_GET = '';
export const URL_COMPANY_PHOTO_SAVE = '';


// AGENT
export const URL_AGENT_LIST = '';
export const URL_AGENT = '';
export const URL_AGENT_CREATE_PARAMS = '';
export const URL_AGENT_CREATE = '';
// /api/v2/agent/edit/<int:agent_id>
export const URL_AGENT_UPDATE = '';
// /api/v2/agent/<int:agent_id>/password
// {
//    "new_password_1": "123456",
//    "new_password_2": "123456"
// }
export const URL_AGENT_PASSWORD_UPDATE = '';
export const URL_AGENT_PHOTO_SAVE = '';



// CLIENT
export const URL_CLIENT_LIST = '';
export const URL_CLIENT_CREATE_PARAMS = '';
export const URL_CLIENT_UPDATE_PARAMS = '';
export const URL_CLIENT_CREATE = '';
export const URL_CLIENT_UPDATE = '';
export const URL_CLIENT_PHOTO_SAVE = '';
// /api/v2/client/get/<int:client_id>




// SALE
export const URL_SALE_CREATE = '';
export const URL_SALE_READ = '';




// FILTER
export const URL_SALE_FILTER_LIST = '';
export const URL_SALE_FILTER_CREATE = '';
export const URL_SALE_FILTER_OBJECTS_COORDS = '';
// Адрес такой же как для списка, только вместо параметров пагинации - ид объекта
export const URL_SALE_FILTER_OBJECT = '';
