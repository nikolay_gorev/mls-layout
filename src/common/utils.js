export const getRandom = (length=getRandom(1)) => +(Math.random() * Math.pow(10, length)).toFixed(0);
getRandom.interval = (min, max) => {
	var rand = min + Math.random() * (max + 1 - min);
	rand = Math.floor(rand);
	return rand;
}
getRandom.str = (length=getRandom(1))=>{
	let alphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя ";
	
	let str = '';
	
	str += alphabet[ getRandom.interval(0, alphabet.length - 1) ].toUpperCase();
	for (var i = 0; i < length - 1; i++) {
		str += alphabet[ getRandom.interval(0, alphabet.length - 1) ];
	}
	
	
	return str;
}
getRandom.date = ()=>{
	return new Date( getRandom.interval(0, +new Date) );
}
getRandom.phone = ()=>{
	return getRandom(10);
}
getRandom.date.str = () => {
	// return '10.10.2010';
	let d = getRandom.date();
	
	let date = d.getDate();
	if (date <= 9) date = '0' + date;
	
	let month = d.getMonth() + 1;
	if (month <= 9) month = '0' + month;
	
	return `${date}.${month}.${d.getFullYear()}`;
}


export const getCookie = name => {
	var matches = document.cookie.match(new RegExp(
		"(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
	));
	
	return matches ? decodeURIComponent(matches[1]) : undefined;
}

export const setCookie = (name, value, options) => {
	options = options || {};
	
	var expires = options.expires;
	
	if (typeof expires == "number" && expires) {
		var d = new Date();
		d.setTime(d.getTime() + expires * 1000);
		expires = options.expires = d;
	}
	if (expires && expires.toUTCString) {
		options.expires = expires.toUTCString();
	}
	
	value = encodeURIComponent(value);
	
	var updatedCookie = name + "=" + value;
	
	for (var propName in options) {
		updatedCookie += "; " + propName;
		var propValue = options[propName];
		if (propValue !== true) {
			updatedCookie += "=" + propValue;
		}
	}
	
	document.cookie = updatedCookie;
}

export const deleteCookie = name => {
	setCookie(name, "", {
		expires: -1
	})
}

export const getCaretPosition = (ctrl) => {
	// IE < 9 Support
	if (document.selection) {
		ctrl.focus();
		var range = document.selection.createRange();
		var rangelen = range.text.length;
		range.moveStart ('character', -ctrl.value.length);
		var start = range.text.length - rangelen;
		return {'start': start, 'end': start + rangelen };
	}
	// IE >=9 and other browsers
	else if (ctrl.selectionStart || ctrl.selectionStart == '0') {
		return {'start': ctrl.selectionStart, 'end': ctrl.selectionEnd };
	} else {
		return {'start': 0, 'end': 0};
	}
}


export const setCaretPosition = (ctrl, start, end) => {
	// IE >= 9 and other browsers
	if(ctrl.setSelectionRange)
	{
		ctrl.focus();
		ctrl.setSelectionRange(start, end);
	}
	// IE < 9
	else if (ctrl.createTextRange) {
		var range = ctrl.createTextRange();
		range.collapse(true);
		range.moveEnd('character', end);
		range.moveStart('character', start);
		range.select();
	}
}


/**
 * Вспомогательная функция для координат относительно документа
 */
export const getCoords = (elem) => {
	var box = elem.getBoundingClientRect();

	var body = document.body;
	var docEl = document.documentElement;

	var scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop;
	var scrollLeft = window.pageXOffset || docEl.scrollLeft || body.scrollLeft;

	var clientTop = docEl.clientTop || body.clientTop || 0;
	var clientLeft = docEl.clientLeft || body.clientLeft || 0;

	var top = box.top + scrollTop - clientTop;
	var left = box.left + scrollLeft - clientLeft;

	return {
		top: top,
		left: left
	};
}

/*export const getParentWithClass = (child, className) => {
	
	if (!child.parentNode.classList) return;
	console.log(child.parentNode.classList.contains(className), child.parentNode.className, className);
	
	if ( child.parentNode.classList.contains(className) )
		return child.parentNode;
	else 
		return getParentWithClass(child.parentNode, className);
}*/

export const getClosest = (node, className) => {
	if (!node.classList) return;
	
	if ( node.classList.contains(className) )
		return node;
	
	return getClosest(node.parentNode, className);
}

export const hasParent = (child, parent) => {
	while(child) {
		if (child === parent) return true;
		
		child = child.parentNode;
	}
	
	return false;
}

/**
Возвращает позицию каретки в инпуте el.
*/
/*getCaretPos: function(el) { 
	if (el.selectionStart) { 
		return el.selectionStart; 
	} else if (document.selection) { 
		el.focus(); 
		
		var r = document.selection.createRange(); 
		if (r == null) {
			return 0; 
		}
		
		var re = el.createTextRange(), 
		rc = re.duplicate(); 
		re.moveToBookmark(r.getBookmark()); 
		rc.setEndPoint("EndToStart", re); 
		
		return rc.text.length; 
	}
	return 0; 
},*/

/**
Устанавливает каретку на позицию pos в инпуте input.
*/
/*setCaretPos: function (input, pos) {
	setSelectionRange(input, pos, pos);
	
	function setSelectionRange(input, selectionStart, selectionEnd) {
		if (input.setSelectionRange) {
			input.focus();
			input.setSelectionRange(selectionStart, selectionEnd);
		}
		else if (input.createTextRange) {
			var range = input.createTextRange();
			range.collapse(true);
			range.moveEnd("character", selectionEnd);
			range.moveStart("character", selectionStart);
			range.select();
		}
	}
}*/

export const indexAmongSiblings = (elem) => {
	let siblings = elem.parentNode.children;
	
	for (var i = 0; i < siblings.length; i++) {
		let sibl = siblings[i];
		
		if (sibl === elem) return i;
	}
}


/*
Принимает объект params, превращает его в строку ?key1=value1&key2=value2.
*/
export const createGetParams = params => {
	if (!params) return '';
	
	let str = '';
	
	let names = Object.keys(params);
	
	if (names.length) {
		for (var i = 0; i < names.length; i++) {
			let name = names[i];
			let value = params[name];
			
			let valueType = {}.toString.call(value).slice(8, -1);
			
			switch(valueType){
				case 'Undefined':
					continue;
				
				case 'Array':
					if (value.length === 0) {
						continue;
					} else {
						value = value.join(',');
					}
			}
			
			if (str[0] !== '?') {
				str += '?';
				
			} else {
				str += '&';
				
			}
			
			str += name + '=' + value;
		}
	}
	
	return str;
}


/*
	debounce
	
	function f1(x) { alert(x) }
	let f = debounce(f1, 1000);

	f(1); // вызов отложен на 1000 мс
	f(2); // предыдущий отложенный вызов игнорируется, текущий (2) откладывается на 1000 мс

	// через 1 секунду появится alert(2)

	setTimeout( function() { f(3) }, 1100); // через 1100 мс отложим вызов еще на 1000 мс
	setTimeout( function() { f(4) }, 1200); // игнорируем вызов (3)

	// через 2200 мс от начала выполнения появится alert(4)
*/
export const debounce = (f, ms) => {

	let timer = null;

	return function (...args) {
		return new Promise((resolve)=>{
			const onComplete = () => {
				let result = f.apply(this, args);
				timer = null;
				resolve(result);
			}

			if (timer) {
				clearTimeout(timer);
			}

			timer = setTimeout(onComplete, ms);
		})
	};
}


export const addMoneySpaces = (value) => {
	const DECIMAL_DELIMITER = '.';
	const DIGIT_DELIMITER = ' ';
	
	let valueRaw = String(value);
	let valueSpaced = '';
	
	
	
	
	// Разделить целую часть от дробной
	let intFraction = valueRaw.split(DECIMAL_DELIMITER);
	let int = intFraction[0];
	let fraction = intFraction[1];
	
	
	
	
	// Расставить пробелы в целой части
	// intSpaced = this.setSpaces(int);
	let intSpaced = '';
	
	let delimiterCounter = 0;
	for (let i = int.length-1; i >= 0; i--) {
		let char = int[i];
		delimiterCounter++;
		
		intSpaced = char + intSpaced;
		if (delimiterCounter === 3) {
			intSpaced = DIGIT_DELIMITER + intSpaced;
			delimiterCounter = 0;
		}
		
	}
	
	intSpaced = intSpaced.trim();
	
	
	
	
	valueSpaced = intSpaced;
	
	if (fraction) {
		// Ограничить дробную часть двумя знаками
		fraction = fraction.slice(0, 2);
		
		valueSpaced += DECIMAL_DELIMITER + fraction;
		
	}
	
	return valueSpaced;
}


export const toNumber = value => {
	switch(value){
		case undefined: return NaN;
	}
	
	let type = {}.toString.call(value).slice(8, -1);
	
	if (type == 'Number') {
		return value;
	}
	
	value = value.replace(',', '.');
	return parseFloat(value);
}


export const log = (value, name, params) => {
	params = params || {};
	
	const ALWAYS = params.always;
	const COLOR = params.color || 'green';
	
	var style = [
	'padding: 1rem 0 0 0;',
	// 'text-decoration: underline;',
	'font: 1rem/1 Tahoma;',
	'color: ' + COLOR + ';'].join('');
	
	name = name || '';
	name += ':';
	
	if (ALWAYS) {
		console.log('%c%s\n', style, name, value);
		
	} else if (~location.search.indexOf('log')) {
		console.log('%c%s\n', style, name, value);
	}
}

log.hash = {
	_values: {},
	
	set(key, value){
		this._values[key] = value;
		
		this._refresh();
	},
	
	_refresh(){
		let hashStr = '';
		
		for(let key in this._values){
			hashStr += `${key}=${this._values[key]};`;
		}
		
		location.hash = hashStr;
	}
}

log.arrDiff = (arr1, arr2) => {
	let loggedValues = [];
	let counter = 0;
	
	console.log('************ ARRAYS DIFF: ************');
	
	arr1.forEach(arr1Elem => {
		let includes = arr2.includes(arr1Elem);
		
		let arr2Elem = includes ? arr1Elem : undefined;
		
		if (includes) {
			// console.log(arr1Elem, arr2Elem);
			
		} else {
			console.log(++counter, arr1Elem, arr2Elem);
			
		}
		
		loggedValues.push(arr1Elem);
	})
	
	
	arr2.forEach(arr2Elem => {
		if (loggedValues.includes(arr2Elem)) return;
		
		
		let includes = arr1.includes(arr2Elem);
		
		let arr1Elem = includes ? arr2Elem : undefined;
		
		if (includes) {
			// console.log(arr1Elem, arr2Elem);
			
		} else {
			console.log(++counter, arr1Elem, arr2Elem);
			
		}
	})
	
	
	console.log('************              ************');
	
	
	// let bothHasElems = arr1.filter(arr1Elem => arr2.includes(arr1Elem));
	
	// // console.log('bothHasElems', bothHasElems);
	
	
	
	// let res1 = arr1.filter(arr1Elem => !bothHasElems.includes(arr1Elem))
	// let res2 = arr2.filter(arr2Elem => !bothHasElems.includes(arr2Elem))
	
	// console.log('res1', res1);
	// console.log('res2', res2);
	
	/*return {
		firstHasNot: res1,
		secondHasNot: res2,
	}*/
};


export const fetch = params => {
	const METHOD = params.method || 'GET';
	const WITH_CREDENTIALS = params.withCredentials || false;
	const URL = params.url;
	const PARAMS = params.params || {};
	const ALERT_ERROR = ('alertError' in params) ? params.alertError : true;
	const LOG_URL = params.logUrl || false;
	
	return new Promise((resolve, reject) => {
		let xhr = new XMLHttpRequest;
		
		xhr.withCredentials = WITH_CREDENTIALS;
		
		let url = URL;
		
		if (METHOD.toLowerCase() === 'get') {
			url += createGetParams(PARAMS);
		}
		
		if (LOG_URL) console.log(url);
		
		xhr.open(METHOD, url);
		
		
		if (METHOD.toLowerCase() === 'get') {
			xhr.send();
			
		} else if (METHOD.toLowerCase() === 'post') {
			xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8')
			xhr.send( JSON.stringify(PARAMS) );
		}
		
		
		xhr.onload = () => {
			if (ALERT_ERROR && (xhr.status < 200 || xhr.status >= 300)) {
				alert(`ERROR (onload)\n\nStatus: ${xhr.status}\nStatus text: ${xhr.statusText}\n\nURL: ${URL}\nMETHOD: ${METHOD}\n\nRESPONSE: ${xhr.response}`);
				
				reject();
			}
			
			try{
				var jsonParsed = JSON.parse(xhr.response)
				
			} catch (e){
				console.error('JSON parse error:', xhr.response)
				reject();
				
				return;
			}
			
			resolve( {
				jsonParsed: jsonParsed,
				response: jsonParsed
			} );
		}
		
		xhr.onerror = () => {
			if (ALERT_ERROR)
				alert(`ERROR (onerror)\n\nStatus: ${xhr.status}\nStatus text: ${xhr.statusText}\n\nURL: ${URL}\nMETHOD: ${METHOD}`);
			
			reject();
		}
	})
}


/*
Класс "переводчик" ключей в json, которые приходят с сервера и используются на фронтенде.
Аргумент map вида {beKey: feKey}
*/
/*export const jsonKeysMap = class {
	constructor(map){
		this.keys = Object.keys(map).sort().map(BEKey => {
			return {
				// key in backend
				be: BEKey,
				
				// key in frontend
				fe: map[BEKey]
			}
		})
	}
	
	getFeKey(beKey){
		let keys = this.keys.find(keys => beKey === keys.be);
		
		return keys && keys.fe;
	}
	
	getBeKey(feKey){
		let keys = this.keys.find(keys => feKey === keys.fe);
		
		return keys && keys.be;
	}
}*/


export const checkJsonSchema = (schema, checkObj) => {
	// Запускает для каждого типа соответствующую функцию проверки
	function runCheck(schema, checkObj) {
		switch (schema.type) {
			case 'object':
				return checkObject(schema, checkObj);
				break;
				
			case 'array':
				return checkArray(schema, checkObj);
				break;
				
			case 'number':
				return checkNumber(schema, checkObj);
				
			case 'string':
				return checkString(schema, checkObj);
				break;
				
			case 'boolean':
				return checkBoolean(schema, checkObj);
				break;
				
		}
	}
	
	function checkObject(schemaObj, checkObj) {
		let type = getType(checkObj);
		
		// Проверка на то что это объект
		if (type !== 'object') {
			errors.add({error: [checkObj, 'is not a object']});
			
			return {
				typeError: true,
				type: type
			}
		}
		
		
		// Проверка наличия обязательных полей
		if (schemaObj.required) {
			schemaObj.required.forEach(key => {
				if (!(key in checkObj)) {
					errors.add({
						error: ['No required', key]
					});
				}
			})
		}
		
		
		// errors.add({title: '\n'});
		
		// Проверка свойств
		if (schemaObj.properties) {
			let required = schemaObj.required || [];
			
			
			for(let key in schemaObj.properties){
				let propSchema = schemaObj.properties[key];
				
				if (key in checkObj) {
					let checkProp = checkObj[key];
					let checkResult = runCheck(propSchema, checkProp);
					
					// Если тип значения свойства не верный - показать ошибку
					if (checkResult.typeError) {
						
						let requiredMess = required.includes(key) ? ', is required' : '';
						
						
						errors.add({
							error: [key, checkProp, 'should be a', propSchema.type, requiredMess]
						})
					} else {
					}
					
				}
			}
		}
	}
	
	function checkArray(schemaArr, checkArr) {
		let type = getType(checkArr);
		
		// Если не массив - показать ошибку
		if (type !== 'array') {
			errors.add({error: [checkArr, 'is not a array']});
			
		} else {
			// Проверка элементов массива
			if (schemaArr.items)
				checkArr.forEach(item => {
					let checkResult = runCheck(schemaArr.items, item)
					
					if (checkResult.typeError) {
						// Если тип элемента не верный - показать ошибку
						errors.add({
							error: [key, checkProp, 'should be a', propSchema.type, 'in array', checkArr]
						})
					}
				})
		}
		
		return {
			typeError: type !== 'array',
			valueType: type
		}
	}
	
	function checkNumber(schemaNum, checkNum) {
		let type = getType(checkNum);
		
		return {
			typeError: type !== 'number',
			valueType: type
		}
	}
	
	function checkString(schemaStr, checkStr) {
		let type = getType(checkStr);
		
		return {
			typeError: type !== 'string',
			valueType: type
		}
	}
	
	function checkBoolean(schemaBool, checkBool) {
		let type = getType(checkBool);
		
		return {
			typeError: type !== 'boolean',
			valueType: type
		}
	}
	
	// Накапливает сообщения, в конце выводит запуском log()
	const errors = {
		messages: [],
		
		add(message){
			this.messages.push(message);
		},
		
		log(){
			this.messages.forEach(m => {
				if (m.title) {
					console.log(m.title);
				}
				
				if (m.error) {
					console.warn.apply(null, m.error);
				}
			})
		}
	}
	
	
	runCheck(schema, checkObj);
	errors.log();
	
	return errors.messages;
}

export const getType = value => {
	return {}.toString.call(value).slice(8, -1).toLowerCase();
}

import IMask from 'imask';

export const toStr = ()=>{};
toStr.phone = value => {
	var mask = IMask(document.createElement('input'), {mask: '+{7}-000-000-00-00'});
	mask.unmaskedValue = value + '';
	
	return mask.value;
}