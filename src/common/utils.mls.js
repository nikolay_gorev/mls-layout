export const mlsDateToDate = mlsDateStr => {
	let mlsDateArr = mlsDateStr.split('.');
	
	// Если дата в формате 1990.10.10
	if (mlsDateArr[0].length === 4) {
		var date = new Date(mlsDateArr[0], mlsDateArr[1] - 1, mlsDateArr[2])
		
	// Если дата в формате 10.10.1990
	} else {
		var date = new Date(mlsDateArr[2], mlsDateArr[1] - 1, mlsDateArr[0])
		
	}
	
	// console.log(mlsDateArr, date);
	
	return date;
}