// AUTH
export const AUTH_SET_USER = 'authSetUser';

// MAP
export const MAP_SET_CENTER = 'setMapCenter';
export const MAP_SET_ZOOM = 'setMapZoom';
export const MAP_SET_ADDS = 'setMapAdds';
// export const MAP_ADD_POLYGON = 'addMapPolygon';
// export const MAP_ADD_PLACEMARK = 'addMapPlacemark';
export const MAP_REMOVE_ALL_GEO_OBJECTS = 'removeAllMapGeoObjects';

// OBJECT CARD GEO
export const OC_SET_REGION_VALUE = 'setRegionValue';
export const OC_SET_ADDRESS_VALUE = 'setAddressValue';
export const OC_SET_ZONE_VALUE = 'setZoneValue';
export const OC_SET_CITY_OR_AROUND_VALUE = 'setCityOrAroundValue';
export const OC_SET_CITY_DISTRICT_VALUE = 'setCityDistrictValue';
export const OC_SET_COMPLEX_VALUE = 'setComplexValue';
export const OC_SET_DIRECTION_VALUE = 'setDirectionValue';
export const OC_SET_REGION_DISTRICT_VALUE = 'setRegionDistrictValue';
export const OC_SET_ADDRESS_GEOPLACE = 'setAddressGeoplace';
export const OC_ADD_METRO_VALUE = 'addMetroValue';
export const OC_REMOVE_METRO_VALUE = 'removeMetroValue';

export const OC_SET_VARIANTS = 'setVariants';
export const OC_SET_LOCATION_VARIANTS = 'setLocationVariants';

export const OC_SET_AREA_TOTAL_VALUE = 'setAreaTotalValue';
export const OC_SET_AREA_LIVING_VALUE = 'setAreaLivingValue';
export const OC_SET_AREA_KITCHEN_VALUE = 'setAreaKitchenValue';
export const OC_SET_AREA_ROOM_1_VALUE = 'setAreaRoom1Value';
export const OC_SET_AREA_ROOM_2_VALUE = 'setAreaRoom2Value';
export const OC_SET_AREA_ROOM_3_VALUE = 'setAreaRoom3Value';

export const OC_SET_PRICE_OBJECT_VALUE = 'setPriceObjectValue';
export const OC_SET_PRICE_METER_VALUE = 'setPriceMeterValue';
export const OC_SET_FEE_VALUE = 'setFeeValue';
export const OC_SET_COUNTERPARTY_BONUS_VALUE = 'setCounterpartyBonusValue';

// export const OC_SET_SELECTED_VALUES = 'setSelectedValues';
export const OC_SET_ADDRESS_VARIANTS = 'setAddressVariants';
export const OC_SET_COMPLEX_VARIANTS = 'setComplexVariants';
export const OC_SET_METRO_VARIANTS = 'setMetroVariants';
export const OC_SET_DIRECTION_VARIANTS = 'setDirecionVariants';
export const OC_SET_CITY_DISTRICT_VARIANTS = 'setCityDistrictVariants';
export const OC_SET_REGION_DISTRICT_VARIANTS = 'setRegionDistrictVariants';
export const OC_SET_REGION_VARIANTS = 'setRegionVariants';
export const OC_SET_CITY_OR_AROUND_VARIANTS = 'setCityOrAroundVariants';
export const OC_SET_ZONE_VARIANTS = 'setZoneVariants';


// OBJECT CARD AGENT
export const OC_SET_AGENT_ID_VALUE = 'setAgentIdValue';
export const OC_SET_AGENTS_VARIANTS = 'setAgentsVariants';


// OBJECT CARD HOUSE TYPE
export const OC_SET_BUILING_AGE_VALUE = 'setBuildingAgeValue';
export const OC_SET_BUILING_YEAR_VALUE = 'setBuildingYearValue';
// export const OC_SET_BUILING_YEAR_FROM_VALUE = 'setBuildingYearFromValue';
// export const OC_SET_BUILING_YEAR_TO_VALUE = 'setBuildingYearToValue';
export const OC_SET_BUILING_QUARTER_VALUE = 'setBuildingQuarterValue';
export const OC_SET_HOUSE_TYPE_VALUE = 'setHouseTypeValue';
export const OC_SET_LAYOUT_TYPE_VALUE = 'setLayoutTypeValue';
export const OC_SET_ROOM_QTY_VALUE = 'setRoomQtyValue';
export const OC_SET_BEDROOM_QTY_VALUE = 'setBedroomQtyValue';
export const OC_SET_ADJOINING_ROOM_QTY_VALUE = 'setAdjoiningRoomQtyValue';
export const OC_SET_FLOORS_QTY_VALUE = 'setFloorsQtyValue';
export const OC_SET_IS_SOCLE_VALUE = 'setIsSocleValue';
export const OC_SET_IS_ATTIC_VALUE = 'setIsAtticValue';
export const OC_SET_FLOOR_FROM_VALUE = 'setFloorFromValue';
export const OC_SET_FLOOR_TO_VALUE = 'setFloorToValue';
export const OC_SET_FLOOR_VALUE = 'setFloorValue';

export const OC_SET_BUILING_AGE_VARIANTS = 'setBuildingAgeVariants';
export const OC_SET_BUILING_YEAR_FROM_VARIANTS = 'setBuildingYearFromVariants';
export const OC_SET_BUILING_YEAR_TO_VARIANTS = 'setBuildingYearToVariants';
export const OC_SET_BUILING_QUARTER_VARIANTS = 'setBuildingQuarterVariants';
export const OC_SET_HOUSE_TYPE_INITIAL_VARIANTS = 'setHouseTypeInitialVariants';
export const OC_SET_LAYOUT_TYPE_INITIAL_VARIANTS = 'setLayoutTypeInitialVariants';
export const OC_SET_ROOM_QTY_VARIANTS = 'setRoomQtyVariants';
export const OC_SET_BEDROOM_QTY_VARIANTS = 'setBedroomQtyVariants';
export const OC_SET_ADJOINING_ROOM_QTY_VARIANTS = 'setAdjoiningRoomQtyVariants';
export const OC_SET_FLOORS_QTY_VARIANTS = 'setFloorsQtyVariants';
export const OC_SET_IS_SOCLE_VARIANTS = 'setIsSocleVariants';
export const OC_SET_IS_ATTIC_VARIANTS = 'setIsAtticVariants';
export const OC_SET_FLOOR_FROM_VARIANTS = 'setFloorFromVariants';
export const OC_SET_FLOOR_TO_VARIANTS = 'setFloorToVariants';

export const OC_RESET_CLIENTS_SEARCH = 'resetClientsSearch';
export const OC_APPLY_CLIENT_FROM_BASE = 'applyClientFromBase';

// OBJECT CARD PHOTOS
export const OC_SET_PHOTOS = 'setOCPhotos';
export const OC_ADD_PHOTO = 'addOCPhoto';
export const OC_DELETE_PHOTO = 'deleteOCPhoto';
export const OC_MOVE_PHOTO = 'movePhoto';


// OBJECT LIST

export const OL_SET_OBJECTS = 'olSetObjects';
export const OL_SET_ZONES = 'olSetZones';
export const OL_SET_CITY_DISTRICTS = 'olSetCityDistricts';
export const OL_SET_REGIONS = 'olSetRegions';
export const OL_SET_REGION_DISTRICTS = 'olSetRegionDistricts';
export const OL_SET_DIRECTONS = 'olSetDirections';
export const OL_SET_CITIES = 'olSetCities';
export const OL_SET_CENTER = 'olSetCenter';
export const OL_SET_ZOOM = 'olSetZoom';
export const OL_SET_PAGE = 'olSetPage';
export const OL_SET_OBJECTS_ON_PAGE = 'olSetObjectsOnPage';
export const OL_SELECT_ALL_OBJECT = 'olSelectAllObject';
export const OL_UNSELECT_ALL_OBJECT = 'olUnselectAllObject';
export const OL_SELECT_OBJECTS = 'olSelectObjects';
export const OL_UNSELECT_OBJECTS = 'olUnselectObjects';
export const OL_SET_MAP_OBJECTS = 'olSetMapObjects';



export const OL_ADD_REGION_DISTRICT = 'olAddRegionDistrict';
export const OL_REMOVE_REGION_DISTRICT = 'olRemoveRegionDistrict';
export const OL_ADD_ZONE = 'olAddZone';
export const OL_REMOVE_ZONE = 'olRemoveZone';
export const OL_INCLUDE_ZONE_IN_CITY_DISTRICT = 'olIncludeZone';
export const OL_EXCLUDE_ZONE_FROM_CITY_DISTRICT = 'olExcludeZone';
export const OL_ADD_CITY_DISTRICT = 'olAddCityDistrict';
export const OL_REMOVE_CITY_DISTRICT = 'olRemoveCityDistrict';
export const OL_ADD_DIRECTION = 'olAddDirection';
export const OL_REMOVE_DIRECTION = 'olRemoveDirection';
// export const OL_TOGGLE_EXCLUDED_ZONE = 'olToggleExcludedZone';



export const OL_TOGGLE_ARE_CITY_DISTRICTS_SELECTABLE = 'olSetCityDistrictsSelectable';
export const OL_TOGGLE_ARE_DIRECTIONS_SELECTABLE = 'olSetDirectionsSelectable';
export const OL_TOGGLE_ARE_REGION_DISTRICTS_SELECTABLE = 'olSetRegionDistrictsSelectable';
export const OL_TOGGLE_ARE_ZONES_SELECTABLE = 'olSetZonesSelectable';



export const OLF_SET_REGION_VALUE = 'setRegionValue';
export const OLF_SET_CITY_OR_NOT_VALUE = 'setCityOrNotValue';
export const OLF_SHOW = 'olfShow';
export const OLF_HIDE = 'olfHide';
export const OLF_SET_FULL_FILTER = 'olfSetFullFilter';
export const OLF_SET_SHORT_FILTER = 'olfSetShortFilter';

export const OLF_SET_OFFER_TYPE = 'olfSetOfferType';
export const OLF_SET_OBJECT_TYPE = 'olfSetObjectType';
export const OLF_SET_ADDS = 'olfSetAdds';
export const OLF_SET_FILTER_SAVED = 'olfSetFilterSaved';

export const OLF_SET_SAVED_LISTS_VARIANTS = 'olfSetSavedListsVariants';
export const OLF_SET_SAVED_LIST = 'olfSetSavedList';
export const OLF_INIT_OBJECTS_PREPARED_TO_DELETE = 'olfInitObjectsPreparedToDelete';
export const OLF_PREPARE_OBJECT_TO_DELETE = 'olfPrepareObjectToDelete';
export const OLF_RESTORE_OBJECT_FROM_DELETED = 'olfRestoreObjectFromDeleted';

export const OLF_SET_BELONGING = 'olfSetBelonging';

// Радиус временно убран
// export const OLF_ADD_RADIUS = 'olfAddRadius';
export const OLF_ADD_CONTOUR = 'olfAddContour';

export const OLF_SET_VALUES = 'olfSetValues';
export const OLF_RESET_VALUES = 'olfResetValues';




// COMPANY
export const COMPANY_SET_VALUES = 'companySetValues';
export const COMPANY_SET_DEFAULTS = 'companySetDefaults';
export const COMPANY_SET_PAGINATION_PAGE = 'companySetPaginationPage';
export const COMPANY_VALUES_RESET = 'companyValuesReset';
export const COMPANY_CREATE_VISIBLE = 'companyCreateVisible';
export const COMPANY_UPDATE_OPEN = 'companyUpdateOpen';




// AGENT
export const AGENT_SET_VALUES = 'agentSetValues';
export const AGENT_SET_DEFAULTS = 'agentSetDefaults';
export const AGENT_UPDATE_OPEN = 'agentUpdateOpen';
export const AGENT_VALUES_RESET = 'agentValuesReset';
export const AGENT_VALUES_VALIDATE = 'agentValuesValidate';
export const AGENT_CREATE_VISIBLE = 'agentCreateVisible';




// CLIENT
export const CLIENT_SET_VALUES = 'clientSetValues';
export const CLIENT_SET_DEFAULTS = 'clientSetDefaults';
export const CLIENT_VALUES_RESET = 'clientValuesReset';
export const CLIENT_CREATE_VISIBLE = 'clientCreateVisible';
