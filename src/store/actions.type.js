export const AUTH_LOGIN = 'authLogin';
export const AUTH_LOGOUT = 'authLogout';

export const FETCH_INITIAL = 'fetchInitial';
export const FETCH_URLS = 'fetchUrls';

// OBJECT CARD
export const OC_FETCH = 'fetchObjectCard';
export const OC_FETCH_OBJECT_VALUES = 'fetchObjectValues';
export const OC_FETCH_GEO = 'fetchGeo';
export const OC_FETCH_AGENT = 'fetchAgent';

export const OC_FETCH_CLIENT_EMAILS = 'fetchClientEmails';
export const OC_FETCH_CLIENTS = 'fetchClients';
export const OC_CREATE_CLIENT_AS_NEW = 'createClientAsNew';

// OBJECTS LIST
export const OL_FETCH_OBJECTS = 'fetchObjects';
export const OL_FETCH_OBJECTS_WITH_COORDS = 'fetchObjectsWithCoords';
export const OL_FETCH_COORDS = 'olFetchCoords';
export const OL_FETCH_SORT = 'olFetchSort';

export const OLF_FETCH_VARIANTS_COMMON = 'olfFetchVariantsCommon';
export const OLF_FETCH_VARIANTS = 'olfFetchVariants';
export const OLF_FETCH_GEO_VARIANTS = 'olfFetchGeoVariants';

export const OLF_SAVE_FILTER = 'olfSaveFilter';
export const OLF_DELETE_SAVED_FILTER = 'olfDeleteFilter';
export const OLF_FETCH_FILTERS_LIST = 'olfFetchFiltersList';
export const OLF_FETCH_SAVED_FILTER_VALUES = 'olfFetchSavedFilterValues';
export const OLF_FETCH_SAVED_FILTER_OBJECTS = 'olfFetchSavedFilterObjects';

export const OLF_FETCH_SAVED_LISTS = 'olfFetchSavedLists';
export const OLF_SAVE_LIST = 'olfSaveList';
export const OLF_UPDATE_LIST = 'olfUpdateSavedList';
export const OLF_FETCH_SAVED_LIST_OBJECTS = 'olfFetchSavedListObjects';
export const OLF_DELETE_SAVED_LIST = 'olfDeleteSavedList';




// COMPANY
export const COMPANY_SAVE = 'companySave';
export const COMPANY_FETCH_FOR_UPDATE = 'companyFetchForUpdate';
export const COMPANY_FETCH_CREATE_PARAMS = 'companyFetchCreateParams';
export const COMPANY_FETCH_LIST = 'companyFetchList';




// AGENT
export const AGENT_SAVE = 'agentSave';
// export const AGENT_FETCH = 'agentFetch';
export const AGENT_FETCH_LIST = 'agentFetchList';
export const AGENT_FETCH_CREATE_PARAMS = 'agentFetchCreateParams';
export const AGENT_FILTER_APPLY = 'agentFilterApply';




// CLIENT
export const CLIENT_SAVE = 'clientSave';
export const CLIENT_FILTER_APPLY = 'clientFilterApply';
export const CLIENT_FETCH_LIST = 'clienttFetchList';
export const CLIENT_FETCH_CREATE_PARAMS = 'clientFetchCreateParams';
export const CLIENT_FETCH_UPDATE_PARAMS = 'clientFetchUpdateParams';




// SALE
// export const SALE_FETCH_CONTROLS = 'saleFetchControls';
