module.exports = {
  pages: {
    'index': {
      entry: './src/main.js',
      template: 'public/index.html',
      filename: 'index.html',
      title: 'Home',
      chunks: [ 'chunk-vendors', 'chunk-common', 'index']
    },
    'Suggestion': {
      entry: './src/main.js',
      template: 'public/index.html',
      title: 'Suggestion',
      chunks: [ 'chunk-vendors', 'chunk-common', 'Suggestion']
    }
  },
  baseUrl: process.env.NODE_ENV === 'production'
    ? './'
    : '/'
}